import { ExtensionContext, workspace, window } from "vscode";
import { getShell, getSessionName } from "./util";
import { spawnSync, SpawnSyncReturns } from "child_process";
import { info, error } from "./log";
import { settings } from "./settings";

/**
 * Navigation direction enum for panes
 * 
 * @export
 * @enum {number} Navigation command input
 */
export type PaneNavigationDirection =
    "active" |
    "next" |
    "previous" |
    "top" |
    "bottom" |
    "left" |
    "right" |
    "top-left" |
    "top-right" |
    "bottom-left" |
    "bottom-right" |
    "up-of" |
    "down-of" |
    "left-of" |
    "right-of"
;


/**
 * Navigation direction enum for windows
 * 
 * @export
 * @enum {number} Navigation command input
 */
export type WindowNavigationDirection =
    "start" |
    "end" |
    "active" |
    "next" |
    "previous";


export default interface MuxProvider {
    sessionExists(): boolean;
    createSession(windowName: string, startingCommand: string): void;
    createWindow(windowName: string, startingCommand: string): void;
    createPane(startingCommand: string, isHorizontal: boolean): void;
    moveToPane(direction: PaneNavigationDirection): void;
    moveToWindow(direction: WindowNavigationDirection): void;
    build(): boolean;
    attach(): void;
    verifyExecutable(): boolean;
    killSession(): SpawnSyncReturns<Buffer>;
}

export class Runnable {
    protected context: ExtensionContext;
    protected readonly providerName: string = this.constructor.name.replace("Provider", "").toLowerCase();
    protected readonly sessionName: string = getSessionName();

    constructor(context: ExtensionContext) {
        this.context = context;
    }

    protected verifyExecutable(): boolean {
        const command = spawnSync('type', ['-a', `${settings.get('executablePath')}`]);

        return command.status !== 0;
    }

    protected runCommand(args: string[], ignoreErrors: boolean = false): SpawnSyncReturns<Buffer> {
        const shell = getShell();
        const executablePath = settings.get('executablePath');
        info(`[${this.providerName}] Running ${executablePath} ${args.join(' ')}`)
        const command = spawnSync(`${executablePath}`, args, { 'shell': shell, 'cwd': workspace.rootPath });

        if (!ignoreErrors && command.status !== 0) {
            error(`"${executablePath} ${args.join(' ')}" gave "${command.stderr.toString().trim() || command.stdout.toString().trim()}" (${command.status})`);
        }

        return command;
    }
}