import { ExtensionContext, workspace, window, Memento, QuickPickItem } from 'vscode';
import { Validator } from 'jsonschema';
import MuxProvider from './provider';
import { info, warning } from './log';
import { MuxConfiguration } from './types';
import { settings } from './settings';
import { spawnSync, SpawnSyncReturns } from 'child_process';
import { getStateProvider, getProjectName, getShell } from './util';
import { join } from 'path';
import { readFileSync } from 'fs';

/**
 * Get all running tmux sessions
 * 
 * @export
 * @param {ExtensionContext} context Context of the extension
 * @returns {string[]} Array of output of `tmux ls` in lines
 */
export function getSessions(): string[] | null {
    const tmuxLs = spawnSync(`${settings.get('executablePath')}`, ['ls']);
    console.log(tmuxLs.stdout.toString(), tmuxLs.stderr.toString());
    
    return tmuxLs.stdout.toString().match(/[^\r\n]+/g);
}


/**
 * Kill all sessions starting with `mux.prefix`
 * 
 * @export
 * @param {ExtensionContext} context Context of the extension
 */
export function killSessions() {
    const sessions = getSessions();
    let items: QuickPickItem[] = [];
    if (sessions === null) {
        window.showErrorMessage("No sessions running");
        return;
    }
    sessions.forEach(session => {
        if (session.startsWith(settings.get('prefix'))) {
            const tokens = session.split(' ');
            items.push({ description: `${tokens[1]} window${tokens[1] === '1' ? '' : 's'}`, label: tokens[0].substring(0, tokens[0].length - 1) });
        }
    });
    window.showQuickPick(items, { placeHolder: 'Select which session to kill' }).then(val => val ? killSession(val.label) : null);
}


/**
 * Navigate through the configuration and create tmux commands to run
 * 
 * @export
 * @param {ExtensionContext} context Context of the extension
 * @returns {boolean} True if all commands exited cleanly, false otherwise
 */
export async function parseArgs(context: ExtensionContext, provider: MuxProvider): Promise<boolean> {
    const stateProvider = getStateProvider(context);
    const configuration = stateProvider.get('configuration') as MuxConfiguration;
    const shell = getShell();
    
    if (configuration) {
        if (configuration.hasOwnProperty('windows')) {
            const windows = configuration['windows'];
            provider.createSession(windows[0].title || windows[0].command || configuration.shell || shell, windows[0].command || configuration.shell || shell);
            if (windows[0].panes != null) {
                windows[0].panes.forEach(pane => {
                    provider.createPane(pane.command || configuration.shell || shell, pane.isHorizontal);
                });
            }
            windows.filter(window => window !== windows[0]).forEach(window => {
                provider.createWindow(window.title || window.command || configuration.shell || shell, window.command || configuration.shell || shell);
                if (window.panes != null) {
                    window.panes.forEach(pane => {
                        provider.createPane(pane.command || configuration.shell || shell, pane.isHorizontal);
                    });
                }
            })
        }
    }

    return provider.build();
}


/**
 * Kill session called `sessionName`
 * 
 * @export
 * @param {string} sessionName 
 * @returns {cp.SpawnSyncReturns<string>} ChildProcess detailing the command
 */
export function killSession(sessionName: string): SpawnSyncReturns<string> {
    console.info(`Killing ${sessionName}`);
    return spawnSync(`${settings.get('executablePath')}`, ['kill-session', '-t', sessionName]);
}

/**
 * Attempt to validate the configuration against the schema.
 * 
 * @param configuration Configuration to validate
 * @returns {boolean} Truthy result on success
 */
export function validate(configuration: MuxConfiguration): boolean {
    const paneSchema = {
        "id": "/Pane",
        "type": "object",
        "properties": {
            "isHorizontal": {
                "type": "boolean",
            },
            "command": {
                "type": "string"
            }
        },
        "required": [
            "isHorizontal"
        ]
    }

    const windowSchema = {
        "id": "/Window",
        "type": "object",
        "properties": {
            "title": {
                "type": "string"
            },
            "command": {
                "type": "string"
            },
            "panes": {
                "type": "array",
                "items": {
                    "$ref": "/Pane"
                }
            }
        }
    }

    const sessionSchema = {
        "id": "/Session",
        "type": "object",
        "properties": {
            "title": {
                "type": "string"
            },
            "shell": {
                "type": "string"
            },
            "windows": {
                "type": "array",
                "items": {
                    "$ref": "/Window"
                }
            }
        },
        "required": [
            "windows"
        ]
    }

    const v = new Validator();
    v.addSchema(sessionSchema, "/Session");
    v.addSchema(windowSchema, "/Window");
    v.addSchema(paneSchema, "/Pane");
    return v.validate(configuration, sessionSchema).valid;
}

/**
 * Load the config into the settings store and validate the config file.
 * 
 * This validation logic will be migrated to logic to live validate the config file
 * 
 * @export
 * @param {ExtensionContext} context Context of the extension
 */
export function loadConfig(context: ExtensionContext) {
    const stateProvider: Memento = getStateProvider(context);
    stateProvider.update('commands', []);
    stateProvider.update('configuration', {});
    // TODO: Add support for contextually checking the global file 
    const filePath = join(workspace.workspaceFolders![0]!.uri.fsPath, '.mux.json');
    const file = JSON.parse(readFileSync(filePath).toString());

    const result = validate(file);

    if (result) {
        info('Using valid project config');
        stateProvider.update('configuration', file);
    } else {
        // TODO: Implement a problem matcher
        warning('Project config is invalid, please check and try again');
        workspace.openTextDocument(filePath);
    }
}

