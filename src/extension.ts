import { getStateProvider, getMuxProvider, getSessionName } from './util';
import { Memento, ExtensionContext, commands, window } from 'vscode';
import * as hash from 'object-hash';
import { error, info } from './log';
import { loadConfig, parseArgs, killSessions, killSession } from './mux';
import MuxProvider from './provider';
import { settings } from './settings';

export function activate(context: ExtensionContext) {
	info('Attempting to load mux');
	
	getMuxProvider(context).then(provider => {
		info(`Using ${typeof(provider)}`);
		
		const stateProvider: Memento = getStateProvider(context);
		const sessionName = getSessionName();
		loadConfig(context);
		stateProvider.update('hash', hash(stateProvider.get('configuration')));
		context.subscriptions.push(
			commands.registerCommand('mux.showMux', () => startup(context, stateProvider, provider)),
			commands.registerCommand('mux.killSessions', () => killSessions()),
			commands.registerCommand('mux.killCurrentSession', () => killSession(sessionName)),
			commands.registerCommand('mux.moveLastActivePane', () => provider.moveToPane("active")),
			commands.registerCommand('mux.moveToNextPane', () => provider.moveToPane("next")),
			commands.registerCommand('mux.moveToPreviousPane', () => provider.moveToPane("previous")),
			commands.registerCommand('mux.moveToTopPane', () => provider.moveToPane("top")),
			commands.registerCommand('mux.moveToBottomPane', () => provider.moveToPane("bottom")),
			commands.registerCommand('mux.moveToLeftPane', () => provider.moveToPane("left")),
			commands.registerCommand('mux.moveToRightPane', () => provider.moveToPane("right")),
			commands.registerCommand('mux.moveToTopLeftPane', () => provider.moveToPane("top-left")),
			commands.registerCommand('mux.moveToTopRightPane', () => provider.moveToPane("top-right")),
			commands.registerCommand('mux.moveToBottomLeftPane', () => provider.moveToPane("bottom-left")),
			commands.registerCommand('mux.moveToBottomRightPane', () => provider.moveToPane("bottom-right")),
			commands.registerCommand('mux.moveToUpOfPane', () => provider.moveToPane("up-of")),
			commands.registerCommand('mux.moveToDownOfPane', () => provider.moveToPane("down-of")),
			commands.registerCommand('mux.moveToLeftOfPane', () => provider.moveToPane("left-of")),
			commands.registerCommand('mux.moveToRightOfPane', () => provider.moveToPane("right-of")),
			commands.registerCommand('mux.moveToLastActiveWindow', () => provider.moveToWindow("active")),
			commands.registerCommand('mux.moveToNextWindow', () => provider.moveToWindow("next")),
			commands.registerCommand('mux.movePreviousWindow', () => provider.moveToWindow("previous")),
			commands.registerCommand('mux.moveToStartWindow', () => provider.moveToWindow("start")),
			commands.registerCommand('mux.moveToEndWindow', () => provider.moveToWindow("end")),
		);
		startup(context, stateProvider, provider);
		info('mux loaded');
		
	}).catch(message => error(`Something went wrong getting the provider in extension activation '${message}'`));
}


/**
 * Create a mux layout 
 * 
 * @param {ExtensionContext} context 
 * @param {Memento} stateProvider 
 */
function startup(context: ExtensionContext, stateProvider: Memento, provider: MuxProvider) {
	const sessionName = getSessionName();

	if (!provider.sessionExists()) {
		parseArgs(context, provider).then(result => {
			if (result && settings.get('attachAtStartup')) {
				provider.attach();
			}
		}).catch(message => error(`Something went wrong getting the provider in extension activation '${message}'`));
	} else {
		window.showErrorMessage(`Duplicate session for ${sessionName}`, { title: 'Restart' }, { title: 'Attach' }).then((clicked: any) => {
			switch (clicked.title) {
				case 'Restart':
					loadConfig(context);
					provider.killSession();
				case 'Attach':
					parseArgs(context, provider).then(result => {
						if (result) {
							provider.attach();
						}
					}).catch(message => error(`Something went wrong getting the provider in extension activation '${message}'`));
					break;
				default:
					break;
			}
		});
	}
}

export function deactivate() { }
