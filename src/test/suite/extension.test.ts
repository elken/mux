import * as assert from 'assert';
import { before, describe, it, beforeEach } from 'mocha';


// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
import * as mux from '../../mux';
import { spawnSync } from 'child_process';
import { join } from 'path';
import { readFileSync } from 'fs';
import { MuxConfiguration } from '../../types';

const prefix: string = 'muxTest';

// import * as myExtension from '../extension';

suite('Extension Test Suite', () => {
	describe('Startup Session', () => {
		mux.killSessions();
		const sessions = mux.getSessions();
		vscode.window.showInformationMessage('Start all tests.');
		console.log(JSON.stringify(sessions));
		
		sessions!.forEach(session => {
			const sessionNumber = sessions!.indexOf(session) + 1;
			const sessionTokens = session.split(' ');
			
			it(`Session #${sessionNumber} should have equal dates`, () => {
				const date = new Date(session.match(/\((.*?)\)/)![1].replace('created', ''));
				assert.equal(date.toDateString(), new Date().toDateString());
			});

			it(`Session #${sessionNumber} should have correct name`, () => {
				assert.equal(sessionTokens[0], `${prefix}-${vscode.workspace.workspaceFolders![0].name}:`);
			});

			it(`Session #${sessionNumber} should have correct window count`, () => {
				const filePath = join(vscode.workspace.workspaceFolders![0]!.uri.fsPath, '.mux.json');
				const file = JSON.parse(readFileSync(filePath).toString()) as MuxConfiguration;

				assert.equal(sessionTokens[1], file.windows.length);
			})
		});
	});
});
