import MuxProvider, { PaneNavigationDirection, WindowNavigationDirection, Runnable } from './provider';
import { ExtensionContext, window, Memento, Terminal } from 'vscode';
import { getShell, getStateProvider, getProjectName } from './util';
import { info, error } from "./log";
import { SpawnSyncReturns } from 'child_process';
import { settings } from './settings';

export default class TmuxProvider extends Runnable implements MuxProvider {
    private stateProvider: Memento;

    constructor(context: ExtensionContext) {
        super(context);
        this.stateProvider = getStateProvider(context);
    }

    /**
     * Create a new tmux session if the current one doesn't exist
     * 
     * @param {string} startingCommand Initial command to run
     * @param {string} windowName Name of the initial window
     * @memberof TmuxProvider
     */
    createSession(windowName: string, startingCommand: string) {
        this.addCommand(['new', '-d', '-s', `'${this.sessionName}'`, '-n', `'${windowName}'`, `'${startingCommand}'` ]);
    }

    /**
     * Create a window within the current session
     * 
     * @param {string} windowName Name of the window
     * @param {string} startingCommand Command to run within the window
     * @memberof TmuxProvider
     */
    createWindow(windowName: string, startingCommand: string) {
        this.addCommand(['new-window', '-n', `'${windowName}'`, startingCommand]);
    }

    /**
     * Create a new pane in the current window
     * 
     * @param {string} startingCommand Initial command to run in the pane
     * @param {boolean} isHorizontal Whether or not to create a horizontal pane or not
     * @memberof TmuxProvider
     */
    createPane(startingCommand: string, isHorizontal: boolean) {
        this.addCommand(['split-window', `-${isHorizontal ? 'h' : 'v'}`, `'${startingCommand}'`]);
    }

    /**
     * Move panes in the given direction
     * 
     * @param {PaneNavigationDirection} direction Direction to move
     * @memberof TmuxProvider
     */
    moveToPane(direction: PaneNavigationDirection) {
        this.runCommand(['select-pane', '-t', `=${this.sessionName}:{${direction}}, '-t', this.sessionName`]);
    }

    /**
     * Moves windows in the given direction
     * 
     * @param {WindowNavigationDirection} direction Direction to move
     * @memberof TmuxProvider
     */
    moveToWindow(direction: WindowNavigationDirection) {
        this.runCommand(['select-window', '-t', `=${this.sessionName}:{${direction}}, '-t', this.sessionName`]);
    }

    /**
     * Check if the session exists
     * 
     * @returns {boolean} True if the session exists
     * @memberof TmuxProvider
     */
    sessionExists(): boolean {
        return this.runCommand(['has-session', '-t', this.sessionName], true).status === 0;
    }

    /**
     * Attach to the session
     * 
     * @memberof TmuxProvider
     */
    attach() {
        const command = `${settings.get('executablePath')} -2 attach-session -t ${this.sessionName}`;
        info(`[tmux] Attaching to session: ${this.sessionName}`);
        window.terminals.filter(t => t.name === this.sessionName).map(t => t.dispose());
        const term = window.createTerminal(this.sessionName);
        term.show();
        term.sendText(command, true);
    }

    /**
     * Ensure that the executable defined is the correct one for the provider
     * 
     * @returns {boolean} True if the executable is correct
     * @memberof TmuxProvider
     */
    verifyExecutable(): boolean {
        const result = this.runCommand(['-V'], true).output.toString().includes('tmux');
        return result;
    }

    /**
     * Add the command to the commands object
     * 
     * @private
     * @param {string} command Arguments to send
     * @memberof TmuxProvider
     */
    private addCommand(command: string[]) {
        const commands: string[][] = this.stateProvider.get('commands') || [];
        this.stateProvider.update('commands', [...commands, [...command]]);
    }

    /**
     * Build a layout from the parsed configuration
     * 
     * @memberof TmuxProvider
     */
    build(): boolean {
        const commands: [][] = this.stateProvider.get('commands') || [];
        return !commands.some(command => {
            const out = this.runCommand(command);
            if (out.stderr) window.showErrorMessage(out.stderr.toString());
            return out.status != 0;
        })
    }

    /**
     * Kill the current session
     * 
     * @memberof TmuxProvider
     */
    killSession(): SpawnSyncReturns<Buffer> {
        return this.runCommand(['kill-session', '-t', this.sessionName]);
    }
}