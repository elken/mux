import { workspace, Uri, ConfigurationTarget } from "vscode";
import { extensionId } from "./constants";

// Most of this borrwed from https://github.com/eamodio/vscode-gitlens

export class Settings {
    get<T>(section?: string, resource?: Uri | null, defaultValue?: T) {
        return defaultValue === undefined
            ? workspace
                  .getConfiguration(section === undefined ? undefined : extensionId, resource!)
                  .get<T>(section === undefined ? extensionId : section)!
            : workspace
                  .getConfiguration(section === undefined ? undefined : extensionId, resource!)
                  .get<T>(section === undefined ? extensionId : section, defaultValue)!;
    }

    update(section: string, value: any, target: ConfigurationTarget, resource?: Uri | null) {
        return workspace
            .getConfiguration(extensionId, target === ConfigurationTarget.Global ? undefined : resource!)
            .update(section, value, target);
    }
}

export const settings = new Settings();