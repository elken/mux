import * as vsc from 'vscode'; 
import MuxProvider from './provider';
import { Providers } from './types';
import { spawnSync } from 'child_process';
import { settings } from './settings';
import { info } from './log';

/**
 * Get the vscode-compatible platform (useful for finding platform settings)
 * 
 * @export
 * @returns {string} vscode-compatible platform
 */
export function getPlatform(): string {
    const platform = process.platform;
    switch(platform) {
        case 'darwin':
            return 'osx';
        default:
            return platform;
    }
}

/**
 * Returns the setting for the default shell
 * 
 * @export
 * @returns {string} The default shell
 */
export function getShell(): string {
    const shellConfig = vsc.workspace.getConfiguration('terminal.integrated.shell');
    const config = shellConfig.get<string>(getPlatform());
    if (config) { return config; }
    const envCommand = spawnSync('env');
    const grepCommand = spawnSync('grep', ['SHELL='], { input: envCommand.stdout });
    const cutCommand = spawnSync('cut', ['-d=', '-f2'], { input: grepCommand.stdout });
    const shell = cutCommand.output[1].slice(0, cutCommand.output[1].length-1).toString();
    if (cutCommand.status === 0 && shell !== '') { 
        return shell; 
    }
    
    return 'sh';
}

/**
 * Get the session name for the current environment
 * 
 * @export
 * @returns {string} Name of the mux session
 */
export function getSessionName(): string {
    return `${settings.get('prefix')}-${getProjectName()}`;
}

/**
 * Return the name of the project (doesn't support mutli-root worksapces yet)
 * 
 * @export
 * @returns {string} The name of the first project in the workspace
 */
export function getProjectName(): string {
    return vsc.workspace.workspaceFolders![0].name || "mux";
}

/**
 * Returns either workspaceState or globalState depending on the current use case
 * 
 * @export
 * @param {ExtensionContext} context Context to run methods on
 * @returns {Memento} workspaceState or globalState object for current context
 */
export function getStateProvider(context: vsc.ExtensionContext): vsc.Memento {
    return context.globalState.get('useWorkspaceState') ? context.workspaceState : context.globalState;
}

/**
 * Construct a Provider based on user settings
 * 
 * @param {ExtensionContext} context Context to run methods on
 * @returns {MuxProvider} Returns a provider promise
 */
export async function getMuxProvider(context: vsc.ExtensionContext): Promise<MuxProvider> {
    const module =  await import (`./${settings.get('provider')}Provider`);
    const provider = Object.create(module.default.prototype);
    return new provider.constructor(context);
}