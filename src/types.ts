export type Providers = "tmux";

export interface MuxConfiguration {
    shell?: string;
    windows: MuxConfigurationWindow[];
}

export interface MuxConfigurationWindow {
    title?: string;
    command?: string;
    panes?: MuxConfigurationPane[];
}

export interface MuxConfigurationPane {
    isHorizontal: boolean;
    command: string;
}